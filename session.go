package hippo

import (
	"crypto/rand"
	"fmt"
	"log"
	"net/http"
	"reflect"
	"time"

	"gitlab.com/gonimals/elephant"
)

var DEVELOPMENT_SESSION *Session

const OneSessionPerUser = false

const CookieName = "Session"

var sessionType reflect.Type

func init() {
	sessionType = reflect.TypeOf((*Session)(nil)).Elem()
}

type Session struct {
	ID     int64 `db:"key"`
	Token  string
	User   int64
	MaxAge time.Time
}

func CreateSession(user *User) (output *Session) {
	output = new(Session)
	output.User = user.ID
	output.MaxAge = time.Now().AddDate(0, 0, 1)
	for output.Token = generateSessionID(); elephant.MainContext.ExistsBy(sessionType, "Token", output.Token); {
		output.Token = generateSessionID() //Just to ensure there are not repeated sessions
	}
	//Invalidate other sessions before inserting the new one
	elephant.MainContext.RetrieveBy(sessionType, "Username", user.ID)
	if OneSessionPerUser {
		oldSession := elephant.MainContext.RetrieveBy(sessionType, "User", user.ID)
		if oldSession != nil {
			oldSession.(*Session).InvalidateSession()
		}
	}
	output.ID = elephant.MainContext.NextID(sessionType)

	return
}

func GetSessionFromRequest(request *http.Request) (output *Session) {
	if DEVELOPMENT_SESSION != nil {
		return DEVELOPMENT_SESSION
	}
	for _, cookie := range request.Cookies() {
		if cookie.Name == CookieName {
			if result := elephant.MainContext.RetrieveBy(sessionType, "Token", cookie.Value); result != nil {
				output = result.(*Session)
			}
			break
		}
	}
	if output != nil {
		if time.Now().After(output.MaxAge) {
			//Session is not valid now
			output.InvalidateSession()
			output = nil
		}
	}
	return
}

func (session *Session) InvalidateSession() {
	err := elephant.MainContext.Remove(sessionType, session.ID)
	if err != nil {
		log.Fatalln("Couldn't invalidate session")
	}
}

func EnableDevelopmentMode() {
	DEVELOPMENT_SESSION = CreateSession(elephant.MainContext.RetrieveBy(userType, "Username", "test").(*User))
}

func generateSessionID() string {
	buffer := make([]byte, hashLength)
	_, err := rand.Read(buffer)
	if err != nil {
		panic("Coudldn't generate random numbers")
	}
	return fmt.Sprintf("%x", buffer)
}
