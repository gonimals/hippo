package hippo

import (
	"bytes"
	"context"
	"encoding/json"
	"log"
	"net/http"
	"os"
	"os/signal"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"syscall"

	"gitlab.com/gonimals/elephant"
)

//web path access control list
type webPathACL struct {
	regex           *regexp.Regexp
	handler         func([]string, http.ResponseWriter, *http.Request)
	handlerSettings []string
	aclName         string
	isOpen          bool
}

type webHandler struct {
}

var webPathACLs []webPathACL //List to be checked on every request
var waitGroup *sync.WaitGroup

//Initialize starts the webserver and allows to configure it
func Initialize(port int, publicCert, privateKey string) {
	if elephant.MainContext == nil {
		log.Fatalln("Can't start with an uninitialized elephant")
	} else if waitGroup != nil {
		log.Fatalln("Hippo already initialized")
	}

	webPathACLs = nil
	AddWebPathACL(regexp.MustCompile("^/login$"), loginHandler, nil, "stdLogin", true)
	var srv http.Server
	srv.Addr = ":" + strconv.Itoa(port)
	srv.Handler = new(webHandler)
	idleConnsClosed := make(chan struct{})
	go func() {
		sigint := make(chan os.Signal, 1)
		signal.Notify(sigint, syscall.SIGINT, syscall.SIGTERM)
		<-sigint
		// We received an interrupt signal, shut down.
		if err := srv.Shutdown(context.Background()); err != nil {
			// Error from closing listeners, or context timeout:
			log.Printf("HTTP server shutdown: %v", err)
		} else {
			log.Println("HTTP server shutdown successful")
		}
		close(idleConnsClosed)
	}()

	waitGroup = new(sync.WaitGroup)
	waitGroup.Add(1)
	go func() {
		if privateKey == "" {
			log.Println(srv.ListenAndServe())
		} else {
			log.Println(srv.ListenAndServeTLS(publicCert, privateKey))
		}
		waitGroup.Done()
	}()
}

//KeepItRunning should be called when the initialization functions are completed
func KeepItRunning() {
	if waitGroup != nil {
		waitGroup.Wait()
	}
	log.Println("Hippo ended running")
}

func (*webHandler) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	log.Printf("[%s] %s", request.Method, request.RequestURI)
	if DEVELOPMENT_SESSION != nil {
		writer.Header().Add("Access-Control-Allow-Origin", "*")
		writer.Header().Add("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS")
		writer.Header().Add("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With")
		if request.Method == http.MethodOptions {
			http.Error(writer, "All methods allowed", http.StatusOK)
			return
		}
	}

	for _, wpa := range webPathACLs {
		if wpa.regex.MatchString(request.URL.Path) {
			if wpa.isOpen {
				wpa.handler(wpa.handlerSettings, writer, request)
				return
			}
			if session := GetSessionFromRequest(request); session != nil {
				user := elephant.MainContext.Retrieve(userType, session.User).(*User)
				for _, permission := range user.Permissions {
					if permission == wpa.aclName {
						wpa.handler(wpa.handlerSettings, writer, request)
						return
					}
				}
			}
			http.Error(writer, "Unathorized", http.StatusUnauthorized)
			return
		}
	}
	http.Error(writer, "We're working on it", http.StatusNotImplemented)
}

//AddWebPathACL adds a new handler to the webserver
func AddWebPathACL(regex *regexp.Regexp, handler func([]string, http.ResponseWriter, *http.Request),
	handlerSettings []string, aclName string, isOpen bool) {
	webPathACLs = append(webPathACLs, webPathACL{regex, handler, handlerSettings, aclName, isOpen})
}

//StaticHandler manages directories and files
//Both settings should not end with '/'
func StaticHandler(handlerSettings []string, writer http.ResponseWriter, request *http.Request) {
	dirRoot := handlerSettings[0]
	uriPath := handlerSettings[1]
	path := dirRoot + strings.Split(request.URL.Path, uriPath)[1]
	stat, err := os.Stat(path)
	if err != nil {
		http.Error(writer, "Not found", http.StatusNotFound)
		return
	}
	mode := stat.Mode() & (os.ModeDir | os.ModeSymlink | os.ModeNamedPipe | os.ModeSocket | os.ModeDevice | os.ModeIrregular)
	if mode != 0 {
		//For regular files, mode is always 0. Directories and other kind of files fall here
		http.Error(writer, "Not found", http.StatusNotFound)
		return
	}
	http.ServeFile(writer, request, path)
	return
}

func userMgmtHandler(handlerSettings []interface{}, writer http.ResponseWriter, request *http.Request) {
	context := elephant.MainContext
	var input inputStruct
	err := json.NewDecoder(request.Body).Decode(&input)
	if err != nil {
		http.Error(writer, "Incorrect request structure", http.StatusBadRequest)
		return
	}
	switch request.Method {
	case http.MethodPut: //Create a new user
		if context.ExistsBy(userType, "Username", input.Username) {
			http.Error(writer, "Can't create that user", http.StatusBadRequest)
			return
		}
		newUser := DefineUser(context.NextID(userType), input.Username, input.Password, input.Context, input.Permissions...)
		context.Create(userType, newUser)
	case http.MethodDelete: //Remove user
		user := context.RetrieveBy(userType, "Username", input.Username)
		if user != nil {
			err = context.Remove(userType, (user.(*User)).ID)
			if err == nil {
				http.Error(writer, "User removed successfully", http.StatusOK)
				return
			}
		}
		http.Error(writer, "Couldn't remove user", http.StatusBadRequest)
	}
	return
}

//LoginHandler is the http function to handle login and logout requests
//This function should be binded to the path "/auth"
func loginHandler(handlerSettings []string, writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Cache-Control", "no-store")
	switch request.Method {
	case http.MethodPost: //This is a login checker. 200 if ok, 401 if not
		if GetSessionFromRequest(request) != nil {
			writer.WriteHeader(http.StatusOK)
		} else {
			writer.WriteHeader(http.StatusUnauthorized)
		}
	case http.MethodPut: //This is a login request. If ok, returns 200 and sets cookie. Returns 401 if not
		var input inputStruct
		err := json.NewDecoder(request.Body).Decode(&input)
		if err != nil {
			http.Error(writer, "Incorrect request structure", http.StatusBadRequest)
			return
		}
		possibleUser := elephant.MainContext.RetrieveBy(userType, "username", input.Username)
		if possibleUser == nil {
			log.Println("No user " + input.Username)
			http.Error(writer, "Incorrect user or password", http.StatusUnauthorized)
			return
		}
		user := possibleUser.(*User)
		if bytes.Compare(generateHash(user.Salt, input.Password), user.Password) != 0 {
			http.Error(writer, "Incorrect user or password", http.StatusUnauthorized)
			return
		}
		session := CreateSession(user)
		http.SetCookie(writer, &http.Cookie{
			Name:     CookieName,
			Value:    session.Token,
			Path:     "/",          //To-Do
			MaxAge:   60 * 60 * 24, //One day max age
			Secure:   DEVELOPMENT_SESSION == nil,
			HttpOnly: true,
			SameSite: http.SameSiteStrictMode,
		})
		//Domain not specified (more restrictive if not specified)
		writer.WriteHeader(http.StatusOK)
		//fmt.Fprintln(writer, session.ID) //Activate this just for debugging purposes
	case http.MethodDelete: //Invalidates the session and clears the cookie
		session := GetSessionFromRequest(request)
		if session != nil {
			session.InvalidateSession()
		}
		http.SetCookie(writer, &http.Cookie{
			Name:   CookieName,
			Value:  "",
			MaxAge: 0,
		}) //This forces user to remove cookie
		writer.WriteHeader(http.StatusOK)
	default:
		http.Error(writer, "Method not allowed", http.StatusMethodNotAllowed)
	}
}

func checkUserAccess(request *http.Request) bool {
	session := GetSessionFromRequest(request)
	if session == nil {
		return false
	}
	requestPath := request.URL.Path + "?" + request.URL.RawQuery
	user := elephant.MainContext.Retrieve(userType, session.User).(*User)
	for _, path := range user.Permissions {
		if strings.HasPrefix(requestPath, path) {
			return true
		}
	}
	return false
}
