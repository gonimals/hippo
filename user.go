package hippo

import (
	"crypto/rand"
	"reflect"

	"golang.org/x/crypto/argon2"

	"gitlab.com/gonimals/elephant"
)

const hashLength = 64

var userType reflect.Type

func init() {
	userType = reflect.TypeOf((*User)(nil)).Elem()
}

type User struct {
	ID          int64 `db:"key"`
	Username    string
	Password    []byte
	Salt        []byte
	Preferences map[string]string // like map[timezone] = Europe/Madrid
	Permissions []string
	Context     string
}

type inputStruct struct {
	Username    string
	Password    string
	Context     string
	Permissions []string
}

func DefineUser(id int64, username, password, context string, permissions ...string) (output *User) {
	output = new(User)
	output.ID = id
	output.Username = username
	output.Salt, output.Password = generateSaltAndHash(password)
	output.Context = context
	output.Preferences = make(map[string]string)
	output.Permissions = permissions[:] //This allows everything
	return
}

func CreateAdminUser(passwd string) {
	context := elephant.MainContext
	tryAdmin := context.Retrieve(userType, 1)
	if tryAdmin != nil {
		return //Existing admin user
	}
	admin := DefineUser(1, "admin", passwd, "", "/usermgmt")
	context.Create(userType, admin)
}

func generateSaltAndHash(password string) (salt, hash []byte) {
	salt = make([]byte, hashLength)
	_, err := rand.Read(salt)
	if err != nil {
		panic("Coudldn't generate random numbers")
	}
	//https://crackstation.net/hashing-security.htm
	hash = generateHash(salt, password)
	return
}

func generateHash(salt []byte, password string) []byte {
	passBytes := make([]byte, len(password))
	copy(passBytes[:], password)
	return argon2.IDKey(passBytes, salt, 1, 64*1024, 8, hashLength)
}
