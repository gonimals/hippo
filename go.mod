module gitlab.com/gonimals/hippo

go 1.17

require (
	gitlab.com/gonimals/elephant v0.0.0-20200429191139-e46dc24a1ce2
	golang.org/x/crypto v0.0.0-20220313003712-b769efc7c000
)

require (
	github.com/mattn/go-sqlite3 v1.14.12 // indirect
	golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect
)
